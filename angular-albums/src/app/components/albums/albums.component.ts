import { Component, OnInit } from '@angular/core';
import {Albums} from "../../models/Albums";

@Component({
  selector: 'app-albums',
  templateUrl: './albums.component.html',
  styleUrls: ['./albums.component.css']
})
export class AlbumsComponent implements OnInit {

album: Albums[];


  constructor() { }

  ngOnInit(): void {
    this.album = [
      {
        title: 'Unplugged in New York',
        artist: 'Nirvana',
        songs: ['Come as You Are' , ' The Man Who Sold the World' , ' About a Girl', ' Polly'],
        favorite: 'Plateau or Lake of Fire',
        year: 1993,
        genre: 'Grunge - Alternative Metal',
        units: 50000000,
        cover: '/assets/images/Nirvana_mtv_unplugged_in_new_york.png'
      },
      {
        title: '40oz to Freedom',
        artist: 'Sublime',
        songs: ['Bad Fish', ' Waiting for My Ruca'],
        favorite: 'Dont Push or Scarlet Begonias',
        year: 1996,
        genre: 'Ska Punk - Reggae Rock',
        units: 20000000,
        cover: '/assets/images/Sublime40OztoFreedomalbumcover.jpg'
      },
      {
        title: 'Saturday Morning: Cartoons\' Greatest Hits',
        artist: 'Helmet, Sublime, the Ramones, Butthole Surfers, Sponge',
        songs: ['Go Speed Racer', ' SpiderMan', ' Underdog', ' Hong Kong Phooey' ],
        favorite: 'Gigantor',
        year: 1995,
        genre: 'Alternative Rock',
        units: 500000,
        cover: '/assets/images/Saturday_Morning.jpg'
      },
      {
        title: 'Life Peachy',
        artist: 'KoRN',
        songs: ['No Place to Hide' , ' A.D.I.D.A.S.' , ' Good God'],
        favorite: 'Wicked',
        year: 1996,
        genre: 'Nu-Metal',
        units: 6500000,
        cover: '/assets/images/Korn-LifeIsPeachy.jpg'
      },
      {
        title: 'Chaos A.D.',
        artist: 'Sepultura',
        songs: ['Refuse/Resist', ' Territory' , ' Slave New World'],
        favorite: 'Refuse/Resist',
        year: 1993,
        genre: 'Groove Metal - Trash Metal',
        units: 925000,
        cover: '/assets/images/Sepultura_-_Chaos_A.D._1993.jpg'
      }

    ]; //END Of ARRAY


  } // end of ngoninit()

} //END OF CLASS DON'T DELETE!!
